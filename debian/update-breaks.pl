#!/usr/bin/env perl

use strict;
use warnings;

while (<>) {
    my @elems = split ' ';

    next if ($elems[0] =~ /^#/);

    if ($elems[2] eq '-') {  # Issue to be fixed
        print "           $elems[0] (>= $elems[1]),\n";
    } else {                            # Already fixed
        print "           $elems[0] (<< $elems[2]),\n";
    }
}
